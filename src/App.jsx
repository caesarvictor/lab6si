import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import React from "react";

function MoviesTable(props) {
  const {list, onDismiss, searchTerm} = props;
  return (
    <table>
        <thead>
          <tr>
            <th>Id</th>
            <th>Título</th>
            <th>Ano</th>
            <th>URL do Poster</th>
          </tr>
        </thead>
        <tbody>
          {list.filter(movie => movie.name.toLowerCase().includes(searchTerm.toLowerCase()))
          .map((movie) => (
            <tr key={movie.id}>
              <td>{movie.id}</td>
              <td>{movie.name}</td>
              <td>{movie.release_year}</td>
              <td>{movie.poster_url}</td>
              <td><Button onClick ={() => onDismiss(movie.id)}>
                Dismiss
                </Button></td>
            </tr>
          ))}
        </tbody>
      </table>
  );
}

function Search(props) {
  const {searchTerm, handleInputChange, children} = props;
  return (
      <form>
        {children} <input type="text" placeholder="Search by movie name"
          name="searchTerm" value={searchTerm} 
          onChange={(event) => handleInputChange(event)}/>
      </form>
    );
}

function Button(props) {
  const {
    onClick,
    className='',
    children,
  } = props;

  return (
    <button
      onClick={onClick}
      className={className}
      type="button"
    >
      {children}
    </button>
  );
}

class App extends React.Component {

  onSearchChange(event) {
    this.setState({ searchTerm: event.target.value });
  }

  constructor(props) {
    super(props);
    this.state = {list: null,
      searchTerm: ''};
    this.onDismiss = this.onDismiss.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = 
      target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  onDismiss(id) {
    const updatedList = this.state.list.filter(item => item.id !== id);
    this.setState({ list: updatedList });
  }

  componentDidMount() {
    fetch('http://127.0.0.1:8000/api/v1/movies/')
      .then((response) => response.json())
      .then((result) => this.setState({ list: result }))
      .catch((error) => console.log(error));
  }

  render() {
    const {list, searchTerm} = this.state;
    
    return (
      <div className="App">
        <Search
          searchTerm={searchTerm}
          handleInputChange={(e) => this.handleInputChange(e)}
        >
          Search term:
        </Search>
        { list ? (
          <MoviesTable 
            list={list} 
            searchTerm={searchTerm}
            onDismiss={(id) => this.onDismiss(id)}
          />
        ) : null}
      </div>
    );
  }
}

export default App;
